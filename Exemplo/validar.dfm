object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Validador de IE'
  ClientHeight = 62
  ClientWidth = 330
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = Ativar
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 87
    Height = 13
    Caption = 'Inscri'#231#227'o estadual'
  end
  object Label2: TLabel
    Left = 191
    Top = 8
    Width = 33
    Height = 13
    Caption = 'Estado'
  end
  object MaskEdit1: TMaskEdit
    Left = 8
    Top = 24
    Width = 177
    Height = 21
    TabOrder = 0
  end
  object Button1: TButton
    Left = 247
    Top = 22
    Width = 75
    Height = 25
    Caption = 'Checar'
    TabOrder = 1
    OnClick = Button1Click
  end
  object ComboBox1: TComboBox
    Left = 191
    Top = 24
    Width = 50
    Height = 21
    ItemIndex = 16
    TabOrder = 2
    Text = 'PE'
    OnSelect = Ativar
    Items.Strings = (
      'AC'
      'AL'
      'AP'
      'AM'
      'BA'
      'CE'
      'DF'
      'ES'
      'GO'
      'MA'
      'MT'
      'MS'
      'MG'
      'PA'
      'PB'
      'PR'
      'PE'
      'PI'
      'RJ'
      'RN'
      'RS'
      'RO'
      'RR'
      'SC'
      'SP'
      'SE'
      'TO')
  end
  object CKIE1: TCKIE
    Left = 128
    Top = 8
  end
end
