unit validar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, CKIE;

type
  TForm1 = class(TForm)
    MaskEdit1: TMaskEdit;
    Button1: TButton;
    ComboBox1: TComboBox;
    CKIE1: TCKIE;
    Label1: TLabel;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Ativar(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Ativar(Sender: TObject);
begin
MaskEdit1.EditMask := CKIE1.Mascara_Inscricao(ComboBox1.Text);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
CKIE1.Inscricao := MaskEdit1.Text;
CKIE1.Estado := ComboBox1.Text;
CKIE1.Validar;
end;

end.
